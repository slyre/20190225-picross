function checkSolution() {
	if (grid)
		if (grid.verify() == true) {
			alert("Tu as gagné !");
		}
	else {
		alert("Raté, essaie encore !");
	}
}



// Class => représente un concept
// new Grid => créer une instance de Grid (correspond à un objet)
class Grid {

	constructor(nbLines, nbCols, hintLines, hintCols) {
		// squares = tableau de cases
		this.nbLines = nbLines;
		this.nbCols = nbCols;
		// tableau de lignes
		this.gameArea = this.constructGrid(nbLines, nbCols);
		this.hintLines = this.addHintLines(hintLines);
		this.hintCols = this.addHintCols(hintCols);
	}

	addHintLines(hintLines) {
		let hintSquaresLines = new Array();
		// Pour les indices des lignes, le num col est toujours 0 
		for (let numLine = 0; numLine < hintLines.length; numLine++) {
			hintSquaresLines[numLine] = new Array();
			// Array to hint pour la ligne
			let currentLineHintArray = hintLines[numLine];
			let startingSquare = this.gameArea[numLine][0];
			// on décale vers la gauche
			let startingX = startingSquare.x - (currentLineHintArray.length * (SQUARE_TOTAL_WIDTH));
			for (let numHint = 0; numHint < currentLineHintArray.length; numHint++) {
				let xCurrentHint = startingX + (numHint * (SQUARE_TOTAL_WIDTH));
				let hintValue = currentLineHintArray[numHint];
				hintSquaresLines[numLine][numHint] = new HintSquare(xCurrentHint, startingSquare.y, hintValue);
			}
		}
		console.log('hintSquaresLines', hintSquaresLines);
		return hintSquaresLines;
	}

	addHintCols(hintCols) {
		let hintSquaresCols = new Array();
		// le num line est toujours 0 
		let lineZero = this.gameArea[0];

		for (let numCol = 0; numCol < hintCols.length; numCol++) {
			hintSquaresCols[numCol] = new Array();
			let currentColHintArray = hintCols[numCol];
			let startingSquare = lineZero[numCol];
			// on décale vers le haut, on prend la case à laquelle on veut rajouter un indice au-dessus en point de départ, ça fait une case, puis on enlève autant de fois la hauteur d'une case qu'on a d'indices
			let startingY = startingSquare.y - (currentColHintArray.length * (SQUARE_TOTAL_HEIGHT));
			// incrémente pour le max d'indices qu'on a dans la colonne
			for (let numHint = 0; numHint < currentColHintArray.length; numHint++) {
				// on part du point Y calculé juste au dessus, auquel on ajoute le n fois la hauteur totale (en fonction du max d'indices) pour décaler de n fois par rapport à la case de départ (dans la zone de jeu)
				let yCurrentHint = startingY + (numHint * (SQUARE_TOTAL_HEIGHT));
				let hintValue = currentColHintArray[numHint]; // la valeur de l'indice (ça peut être 7 ou 4)
				hintSquaresCols[numCol][numHint] = new HintSquare(startingSquare.x, yCurrentHint, hintValue);
			}
		}
		console.log('hintSquaresCols', hintSquaresCols);

		return hintSquaresCols;
	}


	// Renvoie un tableau de "Square" qui correspond à la grille
	constructGrid(nbLines, nbColumns) {
		let gridArray = new Array(nbLines);

		let numSquare = 1;
		for (let numLine = 0; numLine < nbLines; numLine++) {

			// spécifie le nombre de carrés par ligne
			gridArray[numLine] = new Array(nbColumns);

			// incrémentation pour les carrés
			for (let numCol = 0; numCol < nbColumns; numCol++) {

				// Coordonnées x et y pour la case 
				let x = 200 + numCol * (SQUARE_TOTAL_WIDTH);
				let y = 150 + numLine * (SQUARE_TOTAL_HEIGHT);

				// On construit les objets pour conserver l'information et s'en resservir plus tard : par exemple pour changer la couleur de la case
				let square = new Square(numSquare, x, y);
				// Ajoute la case à la grille
				gridArray[numLine][numCol] = square;
				numSquare++;
			}
		}
		// Rend la case numéro 1 invisible
		//gridArray[0][0].clear();
		//console.log('gridArray' , gridArray);
		return gridArray;
	}
	// au lieu d'avoir une ligne indexée par le nombre de colonnes (une seule ligne, plusieurs colonnes), on a une colonne indexée par le nombre de lignes (une seule colonne, plusieurs lignes)
	getCols() {

		let columns = new Array(this.nbCols);

		for (let numLine = 0; numLine < this.nbLines; numLine++) {

			for (let numCol = 0; numCol < this.nbCols; numCol++) {

				// On crée le tableau seulement si il n'existe pas 
				if (!columns[numCol]) {
					columns[numCol] = new Array(this.nbLines);
				}
				let square = this.gameArea[numLine][numCol];

				columns[numCol][numLine] = square;
			}

		}

		return columns;
	}

	// On va s'en servir pour le onclick pour récupérer la case qui correspond l'événement du click
	getSquare(x, y) {

		for (let numLine = 0; numLine < this.nbLines; numLine++) {
			for (let numCol = 0; numCol < this.nbCols; numCol++) {
				let currentSquare = this.gameArea[numLine][numCol]
				let isXInRange = this.isInRange(x, currentSquare.x, currentSquare.x + SQUARE_WIDTH);
				let isYInRange = this.isInRange(y, currentSquare.y, currentSquare.y + SQUARE_HEIGHT);

				if (isXInRange && isYInRange) {
					return currentSquare;
				}
			}
		}
		return;
	}

	isInRange(val, inf, sup) {
		return val >= inf && val <= sup;
	}
	// fonction qui va permettre de vérifier
	verify() {

		for (let numLine = 0; numLine < this.nbLines; numLine++) {
			let line = this.gameArea[numLine];
			// Pour chaque ligne de square : Calculer les suites de cases selectionnées
			let sequence = this.computeSequenceSelectedSquares(line);
			let hintLine = this.hintLines[numLine];

			// comparer avec les indices
			let isEquals = this.compareSequenceAndHints(sequence, hintLine);
			if (!isEquals)
				return false;
		}


		let cols = this.getCols();

		for (let numCol = 0; numCol < this.nbCols; numCol++) {
			let col = cols[numCol];
			let sequence = this.computeSequenceSelectedSquares(col);
			let hintCol = this.hintCols[numCol];
			let isEquals = this.compareSequenceAndHints(sequence, hintCol);
			if (!isEquals)
				return false;


		}

		return true;

		// Pour chaque colonne
	}

	// En entrée : un array de squares / En sortie : tableau de [1,2] 
	computeSequenceSelectedSquares(squares) {

		let sequence = [];
		let counter = 0;

		for (let i = 0; i < squares.length; i++) {
			let square = squares[i];

			if (square.isSelected == true) {
				counter++;
			} else if (square.isSelected == false && counter > 0) {
				sequence.push(counter);
				counter = 0;
			}
			
			if (counter > 0 && i == squares.length - 1) {
				sequence.push(counter);
			}

		}

		if (sequence.length < 1) {
			return [0];
		} else {
			return sequence;
			
		}
		//return sequence.length < 1 ? [0] : sequence;

	}




	// Sequence : array , hint : array de HintSquare
	compareSequenceAndHints(sequence, hints) {
		if (sequence.length != hints.length)
			return false;

		for (let i = 0; i < sequence.length; i++) {
			let currentSequenceElement = sequence[i];
			let hintValue = hints[i].value;

			if (currentSequenceElement != hintValue) {
				return false;
			}
		}

		return true;
	}
}

// Représente une case

class Square {
	// On attribue un numéro de case et les coordonnées de la case
	// Construit une case et la dessine dans le canvas
	constructor(numSquare, x, y) {
		this.numSquare = numSquare;
		this.x = x;
		this.y = y;
		this.isSelected = false;
		// Applique HINTS_COLOR si la condition est remplie, sinon applique GRID_COLOR_AND_RIGHT_CLICK
		// Dessiner la case dans la canvas
		getContext().fillStyle = GRID_COLOR;
		getContext().fillRect(x, y, SQUARE_WIDTH, SQUARE_HEIGHT);
	}

	setColor(color) {
		if (color === RIGHT_CLICK) {
			this.isSelected = false;
		}
		 else {
			this.isSelected = true;
		}
		let context = getContext();
		context.fillStyle = color;
		// square.x => on accède à la propriété x de l'instance de la classe Square
		context.fillRect(this.x, this.y, SQUARE_WIDTH, SQUARE_HEIGHT);
	}

}

class HintSquare extends Square {

	constructor(x, y, value) {
		super(null, x, y);
		this.value = value;
		// console.log('dessiner hintsquare' );
		let context = getContext();
		// Background
		context.fillStyle = HINTS_COLOR;
		context.fillRect(x, y, SQUARE_WIDTH, SQUARE_HEIGHT);
		// Text
		context.font = ".8em Arial";
		context.fillStyle = GRID_COLOR;
		// on divise par deux pour centrer, pour que ça rentre à l'intérieur de la case.
		context.fillText(value, x + (SQUARE_WIDTH / 2), y + (SQUARE_HEIGHT / 2), SQUARE_WIDTH, SQUARE_HEIGHT)
	}

	setColor(color) {
		return;
	}
}