// fonction initialisation de base au chargement de la page
document.addEventListener("click", leftClick);
document.addEventListener("mouseup", rightClick);
window.addEventListener('load', function () {
	//
	context = getContext();
	if (!context) {
		return;
	}


	//
	gameInit();
	grid = new Grid(LINE_TITANIC.length, COL_TITANIC.length, LINE_TITANIC, COL_TITANIC);

}, false);


function clearContext() {
	let context = getContext();
	context.clearRect(0, 0, gameWidth, gameHeight);
}

//facile 
function GridEasy() {
	clearContext();

	document.addEventListener("click", leftClick);
	document.addEventListener("mouseup", rightClick);
	gameInit();
	SQUARE_HEIGHT = 49;
	SQUARE_WIDTH = 49;
	SQUARE_MARGIN = 1;
	SQUARE_TOTAL_WIDTH = SQUARE_WIDTH + SQUARE_MARGIN;
	SQUARE_TOTAL_HEIGHT = SQUARE_HEIGHT + SQUARE_MARGIN;

	grid = new Grid(LINE_TITANIC.length, COL_TITANIC.length, LINE_TITANIC, COL_TITANIC);
}

// difficile 

function GridDifficult() {

	clearContext();
	document.addEventListener("click", leftClick);
	document.addEventListener("mouseup", rightClick);
	gameInit();
	SQUARE_HEIGHT = MUSHROOM_HEIGHT;
	SQUARE_WIDTH = MUSHROOM_WIDTH;
	SQUARE_MARGIN = MUSHROOM_MARGIN;
	SQUARE_TOTAL_WIDTH = SQUARE_WIDTH + SQUARE_MARGIN;
	SQUARE_TOTAL_HEIGHT = SQUARE_HEIGHT + SQUARE_MARGIN;

	grid = new Grid(LINE_MUSHROOM.length, COL_MUSHROOM.length, LINE_MUSHROOM, COL_MUSHROOM);


}



// expert
function GridExpert() {
	clearContext();
	document.addEventListener("click", leftClick);
	document.addEventListener("mouseup", rightClick);
	gameInit();
	SQUARE_HEIGHT = FOREVER_HEIGHT;
	SQUARE_WIDTH = FOREVER_WIDTH;
	SQUARE_MARGIN = FOREVER_MARGIN;
	SQUARE_TOTAL_WIDTH = SQUARE_WIDTH + SQUARE_MARGIN;
	SQUARE_TOTAL_HEIGHT = SQUARE_HEIGHT + SQUARE_MARGIN;

	grid = new Grid(LINE_FOREVER.length, COL_FOREVER.length, LINE_FOREVER, COL_FOREVER);
}

document.oncontextmenu = function () {
	return false;
}