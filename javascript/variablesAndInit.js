let gameWidth = 1600;
let gameHeight = 1600;
let grid;
let nbLines;
let nbColumns;
let SQUARE_MARGIN = 1;
let SQUARE_HEIGHT = 49;
let SQUARE_WIDTH = 49;
const LINE_NUMBER = 6;
const SQUARE_PER_LINE = 6;
let SQUARE_TOTAL_WIDTH = SQUARE_WIDTH + SQUARE_MARGIN;
let SQUARE_TOTAL_HEIGHT = SQUARE_HEIGHT + SQUARE_MARGIN;
const GRID_COLOR = "#D3D3D3";
const RIGHT_CLICK = "#f2f2f2"
const HINTS_COLOR = "#000080";
const LEFT_CLICK = "#000000";
let MUSHROOM_WIDTH = 35;
let MUSHROOM_HEIGHT = 35;
let MUSHROOM_MARGIN = 1;
let FOREVER_WIDTH = 18;
let FOREVER_HEIGHT = 18;
let FOREVER_MARGIN = 1;

const COL_MUSHROOM = [
	[1],
	[2, 1],
	[3, 1],
	[5, 1],
	[2, 7],
	[6, 1],
	[1, 1, 2, 2],
	[10],
	[4, 1],
	[3, 3]
];
const LINE_MUSHROOM = [
	[6],
	[4, 3],
	[3, 5],
	[5, 3],
	[5],
	[4],
	[1, 1],
	[1, 1, 1],
	[1, 2, 4],
	[1, 1, 4, 1]
];



const COL_TITANIC = [
	[0],
	[1],
	[5],
	[4],
	[0]
];

const LINE_TITANIC = [
	[1],
	[2],
	[2],
	[3],
	[2]
];

const COL_FOREVER = [
	[1, 2, 2, 2, 2, 2, 1],
	[1, 1, 2, 2, 2, 2, 2, 1],
	[1, 1],
	[1, 1],
	[1, 1],
	[1, 2, 1],
	[1, 6, 1, 1],
	[1, 6, 2, 1],
	[1, 6, 3, 1],
	[1, 4, 8, 1],
	[1, 3, 5, 2, 1],
	[1, 4, 8, 2, 1],
	[1, 4, 9, 2, 1],
	[1, 4, 11, 1],
	[1, 3, 9, 1],
	[1, 4, 8, 1],
	[1, 6, 3, 1],
	[1, 6, 2, 1],
	[1, 6, 1, 1],
	[1, 2, 1],
	[1, 1],
	[1, 1],
	[1, 1],
	[1, 2, 2, 2, 2, 2, 1, 1],
	[1, 2, 2, 2, 2, 2, 1]
];

const LINE_FOREVER = [
	[1, 2, 2, 2, 2, 2, 1],
	[1, 2, 2, 2, 2, 2, 1, 1],
	[1, 1],
	[1, 1],
	[1, 3, 1],
	[1, 13, 1],
	[1, 13, 1],
	[1, 13, 1],
	[1, 4, 4, 1],
	[1, 4, 3, 4, 1],
	[1, 4, 5, 4, 1],
	[1, 7, 1],
	[1, 7, 1],
	[1, 7, 1],
	[1, 7, 1],
	[1, 1, 5, 1],
	[1, 2, 6, 1],
	[1, 4, 6, 1],
	[1, 6, 6, 1],
	[1, 3, 1],
	[1, 1, 1],
	[1, 1],
	[1, 1],
	[1, 1, 2, 2, 2, 2, 2, 1],
	[1, 2, 2, 2, 2, 2, 1]
];

// retourne l'id grille (dans l'html) avec une définie (de la zone de dessin)
function getCanva() {
	return document.getElementById('grille');
}

// une fois qu'on a le canvas, on s'attaque au contexte qui va être en deux dimensions
function getContext() {
	// on associe la var elem à la function getCanva. Si elem ou elem.getContext false, fin de l'exécution. Sinon, envoie le elem.getContext en 2d.
	var elem = getCanva();

	if (!elem || !elem.getContext) {
		return;
	}

	// spécifie les dimensions du contexte (2d ici)
	return elem.getContext('2d');
}

// lance le jeu
function gameInit() {
	// vérifie que elem et elem.getContext ne renvoient pas de false. détermine les dimensions du jeu
	var elem = getCanva();

	if (!elem || !elem.getContext) {
		return;
	}
	gameWidth = elem.width;
	gameHeight = elem.height;
}