
// Clique droit afin de vider la case

function rightClick(event) {

	let x = event.offsetX;
	let y = event.offsetY;


	// On récupère la case qui correspond aux coordonnées du click de l'utilisateur
	let currentSquare = grid.getSquare(x, y);

	if (event.button == 2) {


		if (currentSquare) {
			currentSquare.setColor(RIGHT_CLICK);

		}
	}
}

// fonction clique gauche qui va colorer la case en noir

function leftClick(event) {

	let x = event.offsetX;
	let y = event.offsetY;

	// On récupère la case qui correspond aux coordonnées du click de l'utilisateur
	let currentSquare = grid.getSquare(x, y);
	if (event.button == 0) {
		if (currentSquare) {
			currentSquare.setColor(LEFT_CLICK);

		}
	}
}